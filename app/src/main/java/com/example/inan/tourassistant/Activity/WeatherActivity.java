package com.example.inan.tourassistant.Activity;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.SearchView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.TextView;

import com.example.inan.tourassistant.API.ApiClient;
import com.example.inan.tourassistant.API.ApiInterface;
import com.example.inan.tourassistant.CurrentWeather.Main;
import com.example.inan.tourassistant.CurrentWeather.Result;
import com.example.inan.tourassistant.CurrentWeather.Weather;
import com.example.inan.tourassistant.Forecast.Forecast;
import com.example.inan.tourassistant.R;
import com.example.inan.tourassistant.Adapter.WeatherAdapter;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.TimeZone;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class WeatherActivity extends AppCompatActivity {

    TextView tempTv,dateTv,mainTv,citynameTv;

    public static final String IMAGE_URL ="http://openweathermap.org/img/w/";
    WeatherAdapter adapter;
    RecyclerView recyclerView;
    LinearLayoutManager layoutManager;
    String appiid = "7fffb16e9b4f308fb2a484c29c732892";
    Toolbar toolbar;
    ApiInterface apiInterface;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_weather);

        toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        getSupportActionBar().setTitle("Weather Report");
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });
        tempTv = findViewById(R.id.temp_tv);
        dateTv = findViewById(R.id.date_tv);
        mainTv = findViewById(R.id.main_tv);
        citynameTv = findViewById(R.id.city_name_tv);
        recyclerView = findViewById(R.id.current_forecast);
        layoutManager = new LinearLayoutManager(this);
        recyclerView.setLayoutManager(layoutManager);
        DividerItemDecoration dividerItemDecoration = new DividerItemDecoration(recyclerView.getContext(),
                layoutManager.getOrientation());
        recyclerView.addItemDecoration(dividerItemDecoration);

        apiInterface = ApiClient.getClient().create(ApiInterface.class);
        String urlString = "weather?q=Dhaka&units=metric&appid=7fffb16e9b4f308fb2a484c29c732892";
        String forecastString ="forecast?q=Dhaka&units=metric&appid=7fffb16e9b4f308fb2a484c29c732892";

        Call<Result> call = apiInterface.getWeather(urlString);

        call.enqueue(new Callback<Result>() {
            @Override
            public void onResponse(Call<Result> call, Response<Result> response) {
                if(response.code()==200){
                    List<Weather> weatherList = response.body().getWeather();
                    citynameTv.setText("Dhaka");
                    Main main = response.body().getMain();
                    tempTv.setText(main.getTemp().toString()+" °C");
                    String weather =weatherList.get(0).getMain();
                    int date = response.body().getDt();
                    Date simpledate = new Date(Long.valueOf(date)*1000);
                    SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
                    sdf.setTimeZone(TimeZone.getTimeZone("GMT-6"));
                    String formattedDate = sdf.format(simpledate);
                    dateTv.setText(formattedDate);
                    mainTv.setText(weather);


                }
            }

            @Override
            public void onFailure(Call<Result> call, Throwable t) {

            }
        });

        Call<Forecast> forecastCall = apiInterface.getForecast(forecastString);

        forecastCall.enqueue(new Callback<Forecast>() {
            @Override
            public void onResponse(Call<Forecast> call, Response<Forecast> response) {
               if(response.code() == 200){
                   List<com.example.inan.tourassistant.Forecast.List> forecastList = response.body().getList();
                   Log.d("forecastList",forecastList.toString());
                   adapter = new WeatherAdapter(forecastList,WeatherActivity.this);
                   recyclerView.setAdapter(adapter);
               }
               else {
                   Log.d("responsecode", String.valueOf(response.code()));
               }
            }

            @Override
            public void onFailure(Call<Forecast> call, Throwable t) {

            }
        });
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater menuInflater = getMenuInflater();
        menuInflater.inflate(R.menu.search_menu, menu);

        MenuItem searchItem = menu.findItem(R.id.action_search);

       SearchView searchView = (SearchView) searchItem.getActionView();
       searchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
           @Override
           public boolean onQueryTextSubmit(String query) {
               getCurrentWeather(query);
               return true;
           }

           @Override
           public boolean onQueryTextChange(String newText) {
               return false;
           }
       });
        return super.onCreateOptionsMenu(menu);
    }

    public void getCurrentWeather(final String query){
        String url = String.format("weather?q=%s&units=metric&appid=7fffb16e9b4f308fb2a484c29c732892",query);
        Call<Result> call = apiInterface.getWeather(url);

        call.enqueue(new Callback<Result>() {
            @Override
            public void onResponse(Call<Result> call, Response<Result> response) {
                if(response.code()==200){
                    List<Weather> weatherList = response.body().getWeather();
                    citynameTv.setText(query);
                    Main main = response.body().getMain();
                    tempTv.setText(main.getTemp().toString()+" °C");
                    String weather =weatherList.get(0).getMain();
                    int date = response.body().getDt();
                    Date simpledate = new Date(Long.valueOf(date)*1000);
                    SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
                    sdf.setTimeZone(TimeZone.getTimeZone("GMT-6"));
                    String formattedDate = sdf.format(simpledate);
                    dateTv.setText(formattedDate);
                    mainTv.setText(weather);
                }
            }

            @Override
            public void onFailure(Call<Result> call, Throwable t) {

            }
        });
        String forecast = String.format("forecast?q=Dhaka&units=metric&appid=7fffb16e9b4f308fb2a484c29c732892",query);
        Call<Forecast> forecastCall = apiInterface.getForecast(forecast);

        forecastCall.enqueue(new Callback<Forecast>() {
            @Override
            public void onResponse(Call<Forecast> call, Response<Forecast> response) {
                if(response.code() == 200){
                    List<com.example.inan.tourassistant.Forecast.List> forecastList = response.body().getList();
                    Log.d("forecastList",forecastList.toString());
                    adapter = new WeatherAdapter(forecastList,WeatherActivity.this);
                    recyclerView.setAdapter(adapter);
                }
                else {
                    Log.d("responsecode", String.valueOf(response.code()));
                }
            }

            @Override
            public void onFailure(Call<Forecast> call, Throwable t) {

            }
        });
    }
}
