package com.example.inan.tourassistant.Adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.example.inan.tourassistant.Model.Event;
import com.example.inan.tourassistant.Interface.OnItemClickListener;
import com.example.inan.tourassistant.R;

import java.util.List;

/**
 * Created by Inan on 3/1/2018.
 */

public class EventAdapter extends RecyclerView.Adapter<EventAdapter.EventViewHolder> {

    Context context;
    List<Event> eventList;
    OnItemClickListener listener;

    public EventAdapter(Context context, List<Event> eventList,OnItemClickListener listener) {
        this.context = context;
        this.eventList = eventList;
        this.listener= listener;
    }

    @Override
    public EventViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
       View view = LayoutInflater.from(context).inflate(R.layout.event_row_item,parent,false);
       final EventViewHolder holder = new EventViewHolder(view);

       view.setOnClickListener(new View.OnClickListener() {
           @Override
           public void onClick(View view) {
               listener.onItemClick(view,holder.getAdapterPosition());
           }
       });
       return holder;
    }

    @Override
    public void onBindViewHolder(EventViewHolder holder, int position) {
        Event event = eventList.get(position);
        holder.eventNameTv.setText(event.getEventName());
        holder.locationTv.setText("Depart From: "+event.getEventLocation());
        holder.destinationTv.setText("Destination: "+event.getEventDestination());
        holder.dateTv.setText("Depart at: "+event.getEventDate());
    }

    @Override
    public int getItemCount() {
        return eventList.size();
    }

    class EventViewHolder extends RecyclerView.ViewHolder{
        TextView eventNameTv,locationTv, destinationTv,dateTv;
        public EventViewHolder(View itemView) {
            super(itemView);
            eventNameTv = itemView.findViewById(R.id.event_name_tv);
            locationTv = itemView.findViewById(R.id.event_location);
            destinationTv = itemView.findViewById(R.id.event_destination);
            dateTv = itemView.findViewById(R.id.departure_date);
        }
    }
}
