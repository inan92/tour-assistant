package com.example.inan.tourassistant.Adapter;

import android.content.Context;
import android.content.DialogInterface;
import android.graphics.Typeface;
import android.support.v7.app.AlertDialog;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseExpandableListAdapter;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.example.inan.tourassistant.Database.DatabaseHelper;
import com.example.inan.tourassistant.Model.Expense;
import com.example.inan.tourassistant.R;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.List;

/**
 * Created by Inan on 3/14/2018.
 */

public class ExpandableAdapter extends BaseExpandableListAdapter {

    private List<String> header_items;
    private HashMap<String,List<String>> child_items;
    Context context;
    int id;

    public ExpandableAdapter(List<String> header_items, HashMap<String, List<String>> child_items, Context context,int id) {
        this.header_items = header_items;
        this.child_items = child_items;
        this.context = context;
        this.id = id;
    }

    @Override
    public int getGroupCount() {
        return header_items.size();
    }

    @Override
    public int getChildrenCount(int i) {
        return child_items.get(header_items.get(i)).size();
    }

    @Override
    public Object getGroup(int i) {
        return header_items.get(i);
    }

    @Override
    public Object getChild(int i, int i1) {
        return child_items.get(header_items.get(i)).get(i1);
    }

    @Override
    public long getGroupId(int i) {
        return i;
    }

    @Override
    public long getChildId(int i, int i1) {
        return i1;
    }

    @Override
    public boolean hasStableIds() {
        return false;
    }

    @Override
    public View getGroupView(int i, boolean b, View view, ViewGroup viewGroup) {

        String title = (String) this.getGroup(i);
        if(view == null){
            LayoutInflater inflater = (LayoutInflater) this.context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            view =inflater.inflate(R.layout.parent_layout,null);
        }
        TextView textView = view.findViewById(R.id.heading_item);
        textView.setTypeface(null, Typeface.BOLD);
        textView.setText(title);
        return view;
    }

    @Override
    public View getChildView(int i, int i1, boolean b, View view, ViewGroup viewGroup) {

        String title = (String) this.getChild(i,i1);
        if(view == null){
            LayoutInflater inflater = (LayoutInflater) this.context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            view =inflater.inflate(R.layout.child_layout,null);
        }
        TextView textView = view.findViewById(R.id.child_item);
        LinearLayout childLayout = view.findViewById(R.id.child_layout);
        textView.setText(title);

        switch (i){
            case 0:
                switch (i1){
                    case 0:
                        childLayout.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View view) {
                                final AlertDialog.Builder builder = new AlertDialog.Builder(context);
                                final View dialogView = LayoutInflater.from(context).inflate(R.layout.custom_dialog,null);
                                final EditText expenseedt = dialogView.findViewById(R.id.expense_name);
                                final EditText expenseamount = dialogView.findViewById(R.id.expense_amount);
                                builder.setView(dialogView)
                                        .setPositiveButton("Enter", new DialogInterface.OnClickListener() {
                                            @Override
                                            public void onClick(DialogInterface dialogInterface, int i) {
                                                String expense_name = expenseedt.getText().toString().trim();
                                                String expense_amount = expenseamount.getText().toString().trim();
                                                SimpleDateFormat formatter = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss");
                                                Date date = new Date();
                                                String date_string = formatter.format(date);
                                                DatabaseHelper helper = new DatabaseHelper(context);
                                                Expense expense = new Expense(expense_name,expense_amount,date_string,String.valueOf(id));
                                               long result = helper.insertExpense(expense);
                                                Log.d("result", String.valueOf(result));

                                            }
                                        })
                                        .setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                                            @Override
                                            public void onClick(DialogInterface dialogInterface, int i) {
                                                dialogInterface.dismiss();
                                            }
                                        }).show();
                            }
                        });
                        break;
                    case 1:
                        childLayout.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View view) {
                                DatabaseHelper helper = new DatabaseHelper(context);
                                List<Expense> expenseList = helper.getAllExpenses(id);

                                Log.d("expenseList",expenseList.get(0).getExpenseName());
                            }
                        });
                        break;
                    case 2:
                       break;
                }
        }
        return view;
    }

    @Override
    public boolean isChildSelectable(int i, int i1) {
        return true;
    }
}
