package com.example.inan.tourassistant.Interface;

import android.view.View;

/**
 * Created by Inan on 3/14/2018.
 */

public interface OnItemClickListener {
    public void onItemClick(View v, int position);
}
