package com.example.inan.tourassistant.API;

import com.example.inan.tourassistant.CurrentWeather.Result;
import com.example.inan.tourassistant.Forecast.Forecast;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Query;
import retrofit2.http.Url;

/**
 * Created by Inan on 3/3/2018.
 */

public interface ApiInterface {

    @GET()
    Call<Result> getWeather(@Url String urlString);

    @GET()
    Call<Forecast> getForecast(@Url String urlString);

}
