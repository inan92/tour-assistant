package com.example.inan.tourassistant.Adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.inan.tourassistant.R;
import com.squareup.picasso.Picasso;

import java.util.List;

import static com.example.inan.tourassistant.Activity.WeatherActivity.IMAGE_URL;

/**
 * Created by Inan on 3/6/2018.
 */

public class WeatherAdapter extends RecyclerView.Adapter<WeatherAdapter.WeatherHolder> {

    List<com.example.inan.tourassistant.Forecast.List> weatherList;
    Context context;

    public WeatherAdapter(List<com.example.inan.tourassistant.Forecast.List> weatherList, Context context) {
        this.weatherList = weatherList;
        this.context = context;
    }

    @Override
    public WeatherHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(context).inflate(R.layout.current_forecast_row_item,parent,false);

        WeatherHolder holder =new WeatherHolder(view);
        return holder;
    }

    @Override
    public void onBindViewHolder(WeatherHolder holder, int position) {
        com.example.inan.tourassistant.Forecast.List list = weatherList.get(position);

        holder.tempTv.setText(list.getMain().getTemp().toString()+" °C");
        holder.humidityTv.setText("Humidity: "+list.getMain().getHumidity().toString()+"%");
        holder.dateTv.setText(list.getDtTxt());
        Picasso.with(context).load(IMAGE_URL+list.getWeather().get(0).getIcon()+".png").into(holder.icon);

        switch (list.getWeather().get(0).getIcon()){
            case "01d":
                holder.tempMain.setText("Clear Sky");
                break;
            case "02d":
                holder.tempMain.setText("Few Clouds");
                break;
            case "01n":
                holder.tempMain.setText("Clear Sky");
                break;
            case "02n":
                holder.tempMain.setText("Few Clouds");
                break;
            case "03d":
                holder.tempMain.setText("Scattered clouds");
                break;
            case "03n":
                holder.tempMain.setText("Scattered clouds");
                break;
            case "04d":
                holder.tempMain.setText("Broken clouds");
                break;
            case "04n":
                holder.tempMain.setText("Broken clouds ");
                break;
            case "09d":
                holder.tempMain.setText("Shower rain");
                break;
            case "09n":
                holder.tempMain.setText("Shower rain");
                break;
            case "10d":
                holder.tempMain.setText("Rain");
                break;
            case "10n":
                holder.tempMain.setText("Rain");
                break;
            case "11d":
                holder.tempMain.setText("Thunderstorm");
                break;
            case "11n":
                holder.tempMain.setText("Thunderstorm");
                break;
            case "13d":
                holder.tempMain.setText("Snow");
                break;
            case "13n":
                holder.tempMain.setText("Snow");
                break;
            case "50d":
                holder.tempMain.setText("Mist");
                break;
            case "50n":
                holder.tempMain.setText("Mist");
                break;


        }
    }

    @Override
    public int getItemCount() {
        return weatherList.size();
    }

    class WeatherHolder extends RecyclerView.ViewHolder{
        TextView tempTv, humidityTv,tempMain,dateTv;
        ImageView icon;
        public WeatherHolder(View itemView) {
            super(itemView);
            tempTv = itemView.findViewById(R.id.temp_tv_day);
            tempMain = itemView.findViewById(R.id.temp_main);
            humidityTv = itemView.findViewById(R.id.humidity_forecast);
            icon = itemView.findViewById(R.id.icon_current_forecast);
            dateTv = itemView.findViewById(R.id.forecast_date);
        }
    }
}
