package com.example.inan.tourassistant.Activity;

import android.app.DatePickerDialog;
import android.content.Intent;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;

import com.example.inan.tourassistant.Database.DatabaseHelper;
import com.example.inan.tourassistant.R;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Locale;

public class AddEventActivity extends AppCompatActivity {

    EditText nameEdt,locationEdt,destinationEdt,budgetEdt,dateEdt;
    Button saveBtn;
     Calendar myCalendar;
    Toolbar toolbar;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_event);

        toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
                overridePendingTransition(R.anim.slide_in_left,R.anim.slide_out_right);
            }
        });
        nameEdt = findViewById(R.id.event_name);
        locationEdt = findViewById(R.id.event_location);
        destinationEdt = findViewById(R.id.event_destination);
        budgetEdt = findViewById(R.id.budget);
        dateEdt = findViewById(R.id.calendar);
        saveBtn = findViewById(R.id.save_event_btn);

         myCalendar = Calendar.getInstance();
        final DatePickerDialog.OnDateSetListener dateSetListener = new DatePickerDialog.OnDateSetListener() {
            @Override
            public void onDateSet(DatePicker datePicker, int i, int i1, int i2) {
                myCalendar.set(Calendar.YEAR,i);
                myCalendar.set(Calendar.MONTH,i1);
                myCalendar.set(Calendar.DAY_OF_MONTH,i2);
                updateLabel();
            }
        };
        dateEdt.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                new DatePickerDialog(AddEventActivity.this,dateSetListener,
                        myCalendar.get(Calendar.YEAR),myCalendar.get(Calendar.MONTH),myCalendar.get(Calendar.DAY_OF_MONTH))
                        .show();
            }
        });

        saveBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String name=nameEdt.getText().toString();
                String location = locationEdt.getText().toString();
                String destination = destinationEdt.getText().toString();
                String date = dateEdt.getText().toString();
                String budget = budgetEdt.getText().toString();
                SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(AddEventActivity.this);
                String email = preferences.getString("email","");
                DatabaseHelper helper = new DatabaseHelper(AddEventActivity.this);
                int id = helper.getUserId(email);
                Log.d("id", String.valueOf(id));
                long result =helper.insertEvent(name,location,destination,budget,date,id);
                Log.d("result", String.valueOf(result));
                startActivity(new Intent(AddEventActivity.this,ProfileActivity.class));
                finish();
            }
        });
    }
    private void updateLabel(){
        String format="dd/MM/yy";
        SimpleDateFormat sdf = new SimpleDateFormat(format,Locale.US);

        dateEdt.setText(sdf.format(myCalendar.getTime()));
    }
}
