package com.example.inan.tourassistant.Database;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteException;
import android.database.sqlite.SQLiteOpenHelper;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Build;
import android.util.Log;

import com.example.inan.tourassistant.Model.Event;
import com.example.inan.tourassistant.Model.Expense;
import com.example.inan.tourassistant.Model.User;

import java.io.ByteArrayOutputStream;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by Inan on 2/23/2018.
 */

public class DatabaseHelper extends SQLiteOpenHelper {

    private static final int DATABASE_VERSION = 1;
    private static final String DATABASE_NAME = "tour_assistant";

    private static final String TABLE_USERS = "user";
    private static final String TABLE_EVENTS = "events";
    private static final String TABLE_EXPENSE = "expenses";

    //user table column names
    private static final String USER_ID  ="user_id";
    private static final String USER_NAME = "user_name";
    private static final String USER_EMAIL = "user_email";
    private static final String USER_PHONE = "user_phone";
    public static final String USER_IMAGE = "user_propic";
    private static final String USER_PASSWORD="user_password";

    //events table column names
    private static final String EVENT_ID="event_id";
    private static final String EVENT_NAME="event_name";
    private static final String EVENT_LOCATION="event_location";
    private static final String EVENT_DESTINATION="event_destination";
    private static final String EVENT_BUDGET="event_budget";
    private static final String EVENT_DATE ="start_date";
    private static final String EVENT_USER_ID = "event_user_id";

    //expense table column names
    private static final String EXPENSE_ID="expense_id";
    private static final String EXPENSE_NAME="expense_name";
    private static final String EXPENSE_AMOUNT="expense_amount";
    private static final String EXPENSE_DATE ="expense_date";
    private static final String EXPENSE_EVENT_ID="expense_event_id";

    public DatabaseHelper(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase sqLiteDatabase) {
        String CREATE_USER_TABLE  = "CREATE TABLE "+TABLE_USERS + "("
                                +USER_ID+ " INTEGER PRIMARY KEY,"+USER_NAME+
                                " TEXT NOT NULL,"+USER_EMAIL+" TEXT NOT NULL,"+USER_PHONE+" TEXT NOT NULL,"
                                +USER_PASSWORD+ " TEXT NOT NULL,"+USER_IMAGE+" BLOB"
                                +")";
        String CREATE_EVENTS_TABLE = "CREATE TABLE "+TABLE_EVENTS+"("
                                +EVENT_ID+" INTEGER PRIMARY KEY,"+EVENT_NAME+
                                " TEXT NOT NULL,"+EVENT_LOCATION+" TEXT NOT NULL,"
                                +EVENT_DESTINATION+" TEXT NOT NULL,"+EVENT_BUDGET+" TEXT NOT NULL,"
                                +EVENT_DATE+" TEXT NOT NULL,"+EVENT_USER_ID+" INTEGER,"
                                +" FOREIGN KEY ("+EVENT_USER_ID+") REFERENCES "+
                                TABLE_USERS+"("+USER_ID+"));";


        String CREATE_EXPENSE_TABLE = "CREATE TABLE "+TABLE_EXPENSE+"("
                                +EXPENSE_ID+" INTEGER PRIMARY KEY,"+EXPENSE_NAME+
                                " TEXT NOT NULL,"+EXPENSE_AMOUNT+" TEXT NOT NULL,"
                                +EXPENSE_DATE+" TEXT NOT NULL,"+EXPENSE_EVENT_ID+" INTEGER,"
                                +" FOREIGN KEY ("+EXPENSE_EVENT_ID+") REFERENCES "+TABLE_EVENTS+"("+EVENT_ID+"));";

        
        sqLiteDatabase.execSQL(CREATE_USER_TABLE);
        sqLiteDatabase.execSQL(CREATE_EVENTS_TABLE);
        sqLiteDatabase.execSQL(CREATE_EXPENSE_TABLE);
    }


    @Override
    public void onConfigure(SQLiteDatabase db) {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN) {
            db.setForeignKeyConstraintsEnabled(true);
        }
    }

    @Override
    public void onUpgrade(SQLiteDatabase sqLiteDatabase, int i, int i1) {
        sqLiteDatabase.execSQL("DROP TABLE IF EXISTS "+TABLE_USERS);
        sqLiteDatabase.execSQL("DROP TABLE IF EXISTS "+TABLE_EVENTS);
        sqLiteDatabase.execSQL("DROP TABLE IF EXISTS "+TABLE_EXPENSE);
        onConfigure(sqLiteDatabase);
    }
    public long insertUser (String name,String phone, String email, String password){
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues values = new ContentValues();
        values.put(USER_NAME,name);
        values.put(USER_PHONE,phone);
        values.put(USER_EMAIL,email);
        values.put(USER_PASSWORD,password);

        long result = db.insert(TABLE_USERS,null,values);
        db.close();
        return result;
    }

    public int insertImage(Bitmap bitmap,String email) {
        ByteArrayOutputStream out = new ByteArrayOutputStream();
        bitmap.compress(Bitmap.CompressFormat.PNG, 100, out);
        byte[] buffer = out.toByteArray();

        SQLiteDatabase db = this.getWritableDatabase();

        db.beginTransaction();
        ContentValues values;

        int id = 0;

        try {
            values = new ContentValues();
            values.put("img", buffer);

            id = db.update(TABLE_USERS,values,USER_EMAIL+"=?",new String[]{email});
            db.setTransactionSuccessful();
            Log.i("Image..", "Inserted..");
        } catch (SQLiteException e) {
            e.printStackTrace();

        } finally {
            db.endTransaction();
            // End the transaction.
            db.close();
            // Close database
        }
        return id;
    }
    public Bitmap getBitmap(String email) {
        Bitmap bitmap = null;
        SQLiteDatabase db = this.getReadableDatabase();

        db.beginTransaction();
        try {
            String selectQuery = "SELECT * FROM " + USER_IMAGE + " WHERE "+USER_EMAIL+" = " + email;
            Cursor cursor = db.rawQuery(selectQuery, null);

            if (cursor.getCount() > 0) {
                while (cursor.moveToNext()) {
                    byte[] blob = cursor.getBlob(cursor.getColumnIndex(USER_IMAGE));
                    bitmap = BitmapFactory.decodeByteArray(blob, 0, blob.length);
                }

            }
            db.setTransactionSuccessful();
        } catch (SQLiteException e) {
            e.printStackTrace();

        } finally {
            db.endTransaction();
            // End the transaction.
            db.close();
            // Close database
        }
        return bitmap;
    }

    public long insertEvent(String name,String location,String destination,String budget,String date,int id){
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues values = new ContentValues();
        values.put(EVENT_NAME,name);
        values.put(EVENT_LOCATION,location);
        values.put(EVENT_DESTINATION,destination);
        values.put(EVENT_BUDGET,budget);
        values.put(EVENT_DATE,date);
        values.put(EVENT_USER_ID,id);

        long result = db.insert(TABLE_EVENTS,null,values);
        db.close();
        return result;
    }
    public List<Event> getAllEvents(int user_id){
        List<Event> eventList = new ArrayList<>();
        SQLiteDatabase db = this.getReadableDatabase();

        Cursor cursor = db.query(TABLE_EVENTS,null,EVENT_USER_ID+"=?",new String[]{String.valueOf(user_id)},null,null,null);
        Log.d("cursor", String.valueOf(cursor.getCount())+user_id);
        while(cursor.moveToNext()){
            String event_id = cursor.getString(cursor.getColumnIndex(EVENT_ID));
            String name = cursor.getString(cursor.getColumnIndex(EVENT_NAME));
            String location = cursor.getString(cursor.getColumnIndex(EVENT_LOCATION));
            String destination = cursor.getString(cursor.getColumnIndex(EVENT_DESTINATION));
            String budget=cursor.getString(cursor.getColumnIndex(EVENT_BUDGET));
            String date = cursor.getString(cursor.getColumnIndex(EVENT_DATE));

            eventList.add(new Event(name,budget,date,location,destination));
        }
        cursor.close();
        return eventList;
    }

public User getUser(String mail){
        User user = null;
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor query = db.query(TABLE_USERS,null,USER_EMAIL+"=?",new String[]{mail},null,null,null);

        while(query.moveToNext()){
        String name = query.getString(query.getColumnIndex(USER_NAME));
        String phone = query.getString(query.getColumnIndex(USER_PHONE));
        String email = query.getString(query.getColumnIndex(USER_EMAIL));
            user= new User(name,phone,email);
        }

        return user;


    }
    public int getUserId(String mail){
        SQLiteDatabase db = this.getReadableDatabase();
        String[] projection = {USER_ID};
        Cursor query = db.query(TABLE_USERS,projection,USER_EMAIL+"=?",new String[] {mail},null,null,null);
        int id = 0;
        while(query.moveToNext()){
             id = query.getInt(query.getColumnIndex(USER_ID));
        }
        return id;
    }

     public long insertExpense(Expense expense){
         ContentValues values = new ContentValues();
         SQLiteDatabase db = this.getWritableDatabase();
         values.put(EXPENSE_NAME,expense.getExpenseName());
         values.put(EXPENSE_AMOUNT,expense.getExpenseAmount());
         values.put(EXPENSE_DATE,expense.getExpenseDate());
         values.put(EXPENSE_EVENT_ID,expense.getEventId());

         long result = db.insert(TABLE_EXPENSE,null,values);

         return result;

    }

    public List<Expense> getAllExpenses(int id){
         SQLiteDatabase db = this.getReadableDatabase();
         List<Expense> expenseList = new ArrayList<>();

         Cursor cursor = db.query(TABLE_EXPENSE,null,EXPENSE_EVENT_ID+"=?",new String[] {String.valueOf(id)},null,null,null);

         while (cursor.moveToNext()){
             String expense_name = cursor.getString(cursor.getColumnIndex(EXPENSE_NAME));
             String expense_amount = cursor.getString(cursor.getColumnIndex(EXPENSE_AMOUNT));
             String expense_date = cursor.getString(cursor.getColumnIndex(EXPENSE_DATE));
             expenseList.add(new Expense(expense_name,expense_amount,expense_date));
         }
         return expenseList;
    }
    public int getEventId(int id){
        SQLiteDatabase db = this.getReadableDatabase();
        String[] projection = {EVENT_ID};
        Cursor cursor = db.query(TABLE_EVENTS,projection,EVENT_USER_ID+"=?",new String[]{String.valueOf(id)},null,null,null);
        int event_id = 0;
        cursor.moveToFirst();
        while (cursor.moveToNext()){
            event_id = cursor.getInt(cursor.getColumnIndex(EVENT_ID));
        }
        return event_id;
    }

}
