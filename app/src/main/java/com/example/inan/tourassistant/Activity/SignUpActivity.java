package com.example.inan.tourassistant.Activity;

import android.content.Intent;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.example.inan.tourassistant.Database.DatabaseHelper;
import com.example.inan.tourassistant.R;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;

public class SignUpActivity extends AppCompatActivity {

    private EditText nameEdt,phoneEdt,emailEdt,passEdt;
    private TextView logInMessage;
    private Button signUpBtn;
    private FirebaseAuth mAuth;
    DatabaseHelper helper;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sign_up);

        mAuth = FirebaseAuth.getInstance();
        helper = new DatabaseHelper(this);
        nameEdt = findViewById(R.id.name_edt);
        phoneEdt = findViewById(R.id.phone_edt);
        emailEdt= findViewById(R.id.sign_up_email);
        passEdt = findViewById(R.id.password_sign_up);
        signUpBtn = findViewById(R.id.sign_up_btn);
        logInMessage = findViewById(R.id.log_in_message);

        signUpBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                final String name = nameEdt.getText().toString();
                final String phone = phoneEdt.getText().toString();
                final String email = emailEdt.getText().toString();
                final String password = passEdt.getText().toString();

                if(name.isEmpty()){
                    nameEdt.setError("You must provide your name");
                    return;
                }
                if(phone.isEmpty()){
                    phoneEdt.setError("You must provide your phone number");
                    return;
                }
                if(!isEmailValid(email)||email.isEmpty()){
                    emailEdt.setError("Please enter a valid email");
                    return;
                }
                if(password.length()<6){
                    passEdt.setError("Password is too short!");
                    return;
                }

                mAuth.createUserWithEmailAndPassword(email,password)
                        .addOnCompleteListener(SignUpActivity.this, new OnCompleteListener<AuthResult>() {
                            @Override
                            public void onComplete(@NonNull Task<AuthResult> task) {
                                if(!task.isSuccessful()){
                                    Toast.makeText(SignUpActivity.this,"Oops! Something went wrong, Please try again"
                                    ,Toast.LENGTH_LONG).show();
                                }
                                else{
                                    startActivity(new Intent(SignUpActivity.this,ProfileActivity.class));
                                    helper.insertUser(name,phone,email,password);
                                    SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(SignUpActivity.this);
                                    SharedPreferences.Editor editor = preferences.edit();
                                    editor.putString("name",name);
                                    editor.putString("email",email);
                                    editor.apply();
                                    finish();
                                }
                            }
                        });
            }
        });
        logInMessage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(SignUpActivity.this,LogInActivity.class));
                finish();
                overridePendingTransition(R.anim.slide_in_left,R.anim.slide_out_right);
            }
        });
    }
    public static boolean isEmailValid(String email) {
        return android.util.Patterns.EMAIL_ADDRESS.matcher(email).matches();
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        startActivity(new Intent(SignUpActivity.this,LogInActivity.class));
        finish();
        overridePendingTransition(R.anim.slide_in_left,R.anim.slide_out_right);
    }
}
