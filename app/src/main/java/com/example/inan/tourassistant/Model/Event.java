package com.example.inan.tourassistant.Model;

/**
 * Created by Inan on 2/23/2018.
 */

public class Event {
    private String eventId;
    private String eventName;
    private String eventBudget;
    private String eventDate;
    private String eventLocation;
    private String eventDestination;

    public String getEventLocation() {
        return eventLocation;
    }

    public void setEventLocation(String eventLocation) {
        this.eventLocation = eventLocation;
    }

    public String getEventDestination() {
        return eventDestination;
    }

    public void setEventDestination(String eventDestination) {
        this.eventDestination = eventDestination;
    }

    public Event(String eventId, String eventName, String eventBudget, String eventDate, String eventLocation, String eventDestination) {
        this.eventId = eventId;
        this.eventName = eventName;
        this.eventBudget = eventBudget;
        this.eventDate = eventDate;
        this.eventLocation = eventLocation;
        this.eventDestination = eventDestination;
    }


    public Event(String eventName, String eventBudget, String eventDate, String eventLocation, String eventDestination) {
        this.eventName = eventName;
        this.eventBudget = eventBudget;
        this.eventDate = eventDate;
        this.eventLocation = eventLocation;
        this.eventDestination = eventDestination;
    }

    public String getEventId() {
        return eventId;
    }

    public void setEventId(String eventId) {
        this.eventId = eventId;
    }

    public String getEventName() {
        return eventName;
    }

    public void setEventName(String eventName) {
        this.eventName = eventName;
    }

    public String getEventBudget() {
        return eventBudget;
    }

    public void setEventBudget(String eventBudget) {
        this.eventBudget = eventBudget;
    }

    public String getEventDate() {
        return eventDate;
    }

    public void setEventDate(String eventDate) {
        this.eventDate = eventDate;
    }


}
