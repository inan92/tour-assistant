package com.example.inan.tourassistant.Activity;

import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.preference.PreferenceManager;
import android.provider.MediaStore;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.NavigationView;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.TextView;

import com.example.inan.tourassistant.Database.DatabaseHelper;
import com.example.inan.tourassistant.Model.Event;
import com.example.inan.tourassistant.Adapter.EventAdapter;
import com.example.inan.tourassistant.Interface.OnItemClickListener;
import com.example.inan.tourassistant.R;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;

import java.util.List;

import de.hdodenhof.circleimageview.CircleImageView;

public class ProfileActivity extends AppCompatActivity {

    Toolbar toolbar;
    TextView create_new_event_Tv;
    private DrawerLayout mDrawerLayout;
    private ActionBarDrawerToggle mDrawerToggle;
    private FirebaseAuth mAuth;
    private NavigationView navigationView;
    private FloatingActionButton addEventBtn;
    CircleImageView circleImageView;
    private int PICK_IMAGE_REQUEST = 1;
    TextView nameTv, emailTv;
    String email;
    String picturePath;
    List<Event> eventList;
    RecyclerView recyclerView;
    LinearLayoutManager manager;
    EventAdapter adapter;
    DatabaseHelper helper;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_profile);
        mAuth=FirebaseAuth.getInstance();
        toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        helper = new DatabaseHelper(this);
        SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(this);
        String name= preferences.getString("name","");
        email = preferences.getString("email","");
        create_new_event_Tv = findViewById(R.id.create_new_event_tv);
        recyclerView = findViewById(R.id.recycler_view);
        final int id = helper.getUserId(email);
        eventList = helper.getAllEvents(id);
       // final
        Log.d("eventList", String.valueOf(eventList.size()));
        manager = new LinearLayoutManager(this);
        adapter = new EventAdapter(this, eventList, new OnItemClickListener() {
            @Override
            public void onItemClick(View v, int position) {
                String name = eventList.get(position).getEventName();
               Intent intent = new Intent(ProfileActivity.this,EventDetailsActivity.class);
               intent.putExtra("name",name);
                int event_id = helper.getEventId(id);
               intent.putExtra("id",event_id);
               startActivity(intent);
            }
        });
        recyclerView.setLayoutManager(manager);
        recyclerView.setAdapter(adapter);


        if(eventList.isEmpty()){
            create_new_event_Tv.setVisibility(View.VISIBLE);
            recyclerView.setVisibility(View.GONE);
        }
        else{
            create_new_event_Tv.setVisibility(View.GONE);
            recyclerView.setVisibility(View.VISIBLE);
        }

        mDrawerLayout = findViewById(R.id.drawer_layout);
        addEventBtn = findViewById(R.id.add_event_btn);

        addEventBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(ProfileActivity.this,AddEventActivity.class));
                overridePendingTransition(R.anim.slide_in_right,R.anim.slide_out_left);
            }
        });


        mDrawerToggle = new ActionBarDrawerToggle(this,mDrawerLayout,R.string.open,R.string.close);
        navigationView = findViewById(R.id.navigation_view);
        mDrawerLayout.addDrawerListener(mDrawerToggle);
        mDrawerToggle.syncState();

        navigationView.setNavigationItemSelectedListener(new NavigationView.OnNavigationItemSelectedListener() {
            @Override
            public boolean onNavigationItemSelected(@NonNull MenuItem item) {

                if(item.isChecked()) item.setChecked(false);
                else
                    item.setChecked(true);

                mDrawerLayout.closeDrawers();
                switch (item.getItemId()) {
                    case R.id.log_out:
                        mAuth.signOut();

                        startActivity(new Intent(ProfileActivity.this, LogInActivity.class));
                        // overridePendingTransition(R.anim.go_down,R.anim.go_up);
                        finish();
                        FirebaseAuth.AuthStateListener authStateListener = new FirebaseAuth.AuthStateListener() {
                            @Override
                            public void onAuthStateChanged(@NonNull FirebaseAuth firebaseAuth) {
                                FirebaseUser user = mAuth.getCurrentUser();
                                if (user == null) {
                                    startActivity(new Intent(ProfileActivity.this, LogInActivity.class));
                                    finish();
                                }
                            }
                        };
                        return true;
                    case R.id.weather:
                        startActivity(new Intent(ProfileActivity.this, WeatherActivity.class));
                        return true;
                }
                return true;
            }
        });
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        View headerView = navigationView.getHeaderView(0);
        circleImageView = headerView.findViewById(R.id.profile_image);
        nameTv = headerView.findViewById(R.id.username);
        emailTv = headerView.findViewById(R.id.email);


        nameTv.setText(name);
        emailTv.setText(email);





        circleImageView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                AlertDialog.Builder builder = new AlertDialog.Builder(ProfileActivity.this);
                builder.setTitle(R.string.pick_action)
                        .setItems(R.array.actions, new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialogInterface, int i) {
                                if(i==0){
                                    Intent takePicture = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
                                    startActivityForResult(takePicture, 0);
                                }else{
                                    Intent pickerPhotoIntent = new Intent(Intent.ACTION_PICK,
                                            MediaStore.Images.Media.EXTERNAL_CONTENT_URI);

                                    startActivityForResult(pickerPhotoIntent,PICK_IMAGE_REQUEST);
                                }
                            }
                        });
                builder.show();


            }
        });
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if(mDrawerToggle.onOptionsItemSelected(item)){
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    protected void onPostCreate(@Nullable Bundle savedInstanceState) {
        super.onPostCreate(savedInstanceState);
        mDrawerToggle.syncState();
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        switch(requestCode) {
            case 0:
                if(resultCode == RESULT_OK){
                    Uri selectedImage = data.getData();
                    circleImageView.setImageURI(selectedImage);

                }

                break;
            case 1:
                if(resultCode == RESULT_OK){
                    DatabaseHelper helper = new DatabaseHelper(this);
                    Uri selectedImage = data.getData();

                    String[] filePathColumn = {MediaStore.Images.Media.DATA};
                    Cursor cursor = getContentResolver().query(selectedImage,filePathColumn,null,null,null);

                    assert cursor != null;
                    cursor.moveToFirst();

                    int columnIndex = cursor.getColumnIndex(filePathColumn[0]);
                    picturePath = cursor.getString(columnIndex);
                    cursor.close();
                    Bitmap bm = BitmapFactory.decodeFile(picturePath);
                        //circleImageView.setImageBitmap(bitmap)
                    getImage(bm);
                }
                break;
        }
    }
    public void getImage(Bitmap bm) {


        DatabaseHelper helper = new DatabaseHelper(this);
        int id = (int) helper.insertImage(bm,email);
        circleImageView.setImageBitmap(helper.getBitmap(email));
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        finish();
    }
}

