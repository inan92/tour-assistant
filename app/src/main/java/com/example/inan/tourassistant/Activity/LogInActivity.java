package com.example.inan.tourassistant.Activity;

import android.content.Intent;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.example.inan.tourassistant.Database.DatabaseHelper;
import com.example.inan.tourassistant.R;
import com.example.inan.tourassistant.Model.User;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;

public class LogInActivity extends AppCompatActivity {


    EditText emaileEdt, passwordEdt;
    Button loginBtn,sign_up_btn;
    private FirebaseAuth mAuth;
    DatabaseHelper helper;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_log_in);

        mAuth = FirebaseAuth.getInstance();
        helper = new DatabaseHelper(this);
        if(mAuth.getCurrentUser()!=null){
            startActivity(new Intent(LogInActivity.this,ProfileActivity.class));
            finish();
        }
        sign_up_btn = findViewById(R.id.go_to_sign_up_btn);

        emaileEdt = findViewById(R.id.email_edt);
        passwordEdt = findViewById(R.id.pasword_edt);

        loginBtn = findViewById(R.id.log_in_btn);

        sign_up_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(LogInActivity.this,SignUpActivity.class));
                finish();
                overridePendingTransition(R.anim.slide_in_right,R.anim.slide_out_left);
            }
        });

        loginBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                final String email = emaileEdt.getText().toString();
                String password = passwordEdt.getText().toString();
                if(!isEmailValid(email)){
                    emaileEdt.setError("Please enter a valid email");
                }
                if(password.length() < 6){
                    passwordEdt.setError("Password is too short");
                }

                mAuth.signInWithEmailAndPassword(email,password)
                        .addOnCompleteListener(LogInActivity.this, new OnCompleteListener<AuthResult>() {
                            @Override
                            public void onComplete(@NonNull Task<AuthResult> task) {
                                if(!task.isSuccessful())
                                {
                                    Toast.makeText(LogInActivity.this,"Oops! Something went wrong, Please try again",
                                            Toast.LENGTH_LONG).show();
                                }
                                else{
                                    startActivity(new Intent(LogInActivity.this,ProfileActivity.class));
                                    overridePendingTransition(R.anim.go_up,R.anim.go_down);
                                    User user = helper.getUser(email);
                                    String name= user.getName();
                                    Log.d("name",name);
                                    Log.d("email",email);
                                    SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(LogInActivity.this);
                                    SharedPreferences.Editor editor = preferences.edit();
                                    editor.putString("name",name);
                                    editor.putString("email",email);
                                    editor.apply();
                                    finish();
                                }
                            }
                        });

            }
        });


    }
    public static boolean isEmailValid(String email) {
        return android.util.Patterns.EMAIL_ADDRESS.matcher(email).matches();
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        finish();
    }
}
