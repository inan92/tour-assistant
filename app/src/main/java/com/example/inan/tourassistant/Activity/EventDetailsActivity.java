package com.example.inan.tourassistant.Activity;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.View;
import android.widget.ExpandableListView;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.example.inan.tourassistant.Adapter.ExpandableAdapter;
import com.example.inan.tourassistant.R;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public class EventDetailsActivity extends AppCompatActivity {

    TextView eventnameTv;
    List<String> headings;
    List<String> list1;
    List<String> list2;
    List<String> list3;
    HashMap<String,List<String>> childList;
    ExpandableAdapter adapter;
    ExpandableListView expandableListView;

    DisplayMetrics metrics;
    int width;
    Toolbar toolbar;
    ProgressBar progressbar;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_event_details);
        toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });
        eventnameTv = findViewById(R.id.event_nameTv);
        expandableListView = findViewById(R.id.expandble_listview);
        progressbar = findViewById(R.id.progressbar);
        progressbar.setMax(100);
        progressbar.setProgress(100);
        metrics = new DisplayMetrics();
        getWindowManager().getDefaultDisplay().getMetrics(metrics);
        width = metrics.widthPixels;
        expandableListView.setIndicatorBounds(width-GetDipsFromPixel(50), width - GetDipsFromPixel(10));
        headings = new ArrayList<>();
        list1 = new ArrayList<>();
        list2 = new ArrayList<>();
        list3 = new ArrayList<>();
        childList = new HashMap<>();
        String name = getIntent().getStringExtra("name");
        int id = getIntent().getIntExtra("id",2);
        Log.d("id", String.valueOf(id));
        eventnameTv.setText(name);

        String headingitems[] = getResources().getStringArray(R.array.header_titles);
        String l1[] = getResources().getStringArray(R.array.ex_items);
        String l2[] = getResources().getStringArray(R.array.moments_items);
        String l3[] = getResources().getStringArray(R.array.more_items);

        for (String title:headingitems) {

            headings.add(title);
        }
        for(String title:l1){
            list1.add(title);
        }
        for(String title:l2){
            list2.add(title);
        }
        for(String title:l3){
            list3.add(title);
        }

        childList.put(headings.get(0),list1);
        childList.put(headings.get(1),list2);
        childList.put(headings.get(2),list3);

        adapter = new ExpandableAdapter(headings,childList,this,id);
        expandableListView.setAdapter(adapter);

    }

    public int GetDipsFromPixel(float pixels)
    {
        // Get the screen's density scale
        final float scale = getResources().getDisplayMetrics().density;
        // Convert the dps to pixels, based on density scale
        return (int) (pixels * scale + 0.5f);
    }
}
